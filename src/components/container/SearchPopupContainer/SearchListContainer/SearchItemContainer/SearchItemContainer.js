import React, {Component} from 'react';
import ItemDetail from '../../../../UI/ItemDetail';
import PersonItem from '../../../../UI/PersonItem';

const baseUrlImg = process.env.MOVIE_IMG_BASE_URL;
const ItemTypes = (item, style, baseUrlImg) => ({
    movie: <ItemDetail item={item} classes={style} baseUrlImg={baseUrlImg}/>,
    tv: <ItemDetail item={item} classes={style} baseUrlImg={baseUrlImg}/>,
    person: <PersonItem item={item} classes={style} baseUrlImg={baseUrlImg}/>,
    trending: <ItemDetail item={item} classes={style} baseUrlImg={baseUrlImg}/>
});


class SearchItemContainer extends Component {

    render() {
        const {classes, itemInfo} = this.props;
        const itemSearchType = itemInfo.media_type || 'trending';
        return (
            <React.Fragment>
                { itemInfo ? ItemTypes(itemInfo, classes, baseUrlImg)[itemSearchType] : <div><h4>No results</h4></div>}
            </React.Fragment>
        );
    }
}
export default SearchItemContainer;