import React, { Component } from 'react';
import injectSheet from 'react-jss';
import MovieAPI from '../../../api/discover/movie';
import GalleryItemCard from '../../UI/GalleryItemCard';
import { styles } from './styles';
import type { ItemSearchType } from '../../types/appTypes';

const imgBaseUrl = process.env.MOVIE_IMG_BASE_URL;

type HomeStateType = {
    results?: Array<ItemSearchType>,
    loaded: boolean
};

/*
*  This component container fetchs data and send to presentation component GalleryItemCard
* */
class HomeContainer extends React.Component<{}, HomeStateType> {
    constructor(props: React.Node) {
        super(props);
        this.state = {
            results: [],
            loaded: false,
        };
        //$FlowFixMe
        this.handleCardClick = this.handleCardClick.bind(this);
    }

    /*
        call api movies and load best vote_average results
     */
    componentDidMount() {
        MovieAPI()
            .then((resultsFetched) => {
                this.setState({
                    results: resultsFetched.results,
                    loaded: true,
                });
            })
            .catch((reject) => {
                this.setState({
                    loaded: false,
                });
                console.error('Error discover movie api ', reject);
            });
    }

    /*
        HandleClick on card trigger a route change to item detail with his information
     */
    handleCardClick(item) {
        this.props.history.push({
            pathname: '/detail',
            state: item,
        });
    }

    render() {
        const { results, loaded } = this.state;
        const { classes } = this.props;
        return (
            <React.Fragment>
                {loaded && <h2>Best rated movies</h2>}
                <div className={classes.containerItems}>
                    {results.map((itemData) =>
                        <div key={itemData.id} onClick={() => this.handleCardClick(itemData)}>
                            <GalleryItemCard classes={classes}
                                             item={itemData}
                                             baseUrlImg={imgBaseUrl}/>
                        </div>,
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default injectSheet(styles)(HomeContainer);