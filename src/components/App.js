import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import injectSheet from 'react-jss';
import HeaderContainer from './container/HeaderContainer';
import HomeContainer from './container/HomeContainer';
import ItemDetailsContainer from './container/ItemDetailsContainer';
import { styles } from './styles';

class App extends Component {
    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <header className={classes.header}>
                    <HeaderContainer/>
                </header>
                <main className={classes.mainContainer}>
                    <Switch>
                        <Route exact path="/" component={HomeContainer}/>
                        <Route path="/detail" component={ItemDetailsContainer}/>
                    </Switch>
                </main>
                <footer className={classes.footer}>
                    <p>Copyright 2018 Giuseppe Scuglia</p>
                </footer>
            </React.Fragment>
        );
    }
}

export default injectSheet(styles)(App);
