export const styles = {
    formWrapper: {
        color: '#222',
        backgroundColor: '#ffffff',
        width: '100%',
        height: 40,
        margin: '0 20% 0 10%',
    },
    searchSubmitIcon: {
        color: '#01D277',
        fontSize: 20,
        paddingLeft: 20,
    },
    searchDeleteIcon: {
        color: '#01D277',
        fontSize: 20,
        paddingRight: 20,
    },
    searchForm: {
        display: 'flex',
        alignItems: 'center',
        width: '100%',
        '& > input': {
            backgroundColor: 'transparent',
            border: 'none',
            boxSizing: 'border-box',
            height: 40,
            fontFamily: 'inherit',
            fontSize: 18,
            lineHeight: 1.2,
            outline: 'none',
            padding: `14px`,
            width: '100%',
            color: '#01D277',
            '&::placeholder': {
                color: '#01D277',
            },
        },
    },
    '@media (max-width: 767px)': {
        formWrapper: {
            margin: 0
        }
    },
};