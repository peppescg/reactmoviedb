export const styles = {
    footer: {
        backgroundColor: '#081c24',
        color: '#01D277',
        position: 'absolute',
        right: 0,
        bottom: 0,
        left: 0,
        paddingLeft: 20,
        borderTop: '1px solid #01D277'
    },
    header: {
        height: 60
    },
    mainContainer: {
        padding: '40px 0 80px 0',
        '& > h2': {
            textAlign: 'center'
        }
    },
    headerWrapper: {
        backgroundColor: '#081c24',
        color: '#01D277',
        margin: 0,
        display: 'flex',
        alignItems: 'center',
        borderBottom: '1px solid #01D277',
        position: 'fixed',
        width: '100%',
        justifyContent: 'center'
    },
    logoWrapper: {
        marginLeft: 50,
        display: 'flex',
        alignItems: 'center',
        '& > a > h1': {
            margin: 0,
        },
        '& > a': {
            color: 'inherit',
            textDecoration: 'none'
        }
    },
    '@media (max-width: 767px)': {
        headerWrapper: {
            flexDirection: 'column'
        },
        logoWrapper: {
            padding: 0
        }
    },
};