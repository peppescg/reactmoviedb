import injectSheet from 'react-jss';
import HeaderContainer from './HeaderContainer';
import { styles } from './styles';

export default  injectSheet(styles)(HeaderContainer);
