import React from 'react';
import renderer from 'react-test-renderer';
import GalleryItemCard from '../GalleryItemCard';

const item = {
  title: 'title',
  vote_average: '8',
  release_date: '2018-4-8',
  poster_path: '/path'
};


it('renders correctly', () => {
  const tree = renderer
    .create(<GalleryItemCard classes={{}} item={item} baseUrlImg={'localhost:8080'} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});