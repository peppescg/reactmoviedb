export const styles = {
    card: {
        boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
        transition: '0.3s',
        height: 540,
        margin: 5,
        width: 300,
        '&:hover': {
            boxShadow: '0px 3px 3px 5px rgba(0,0,0,0.2)',
        },
        cursor: 'pointer'
    },
    image: { width: 300 },
    titleContainer: {
        padding: 5,
        '& > h4': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            margin: 0,
            padding: 0,
        },
    },
    average: {
        width: 70,
        paddingLeft: 20,
    },
    containerItems: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
};