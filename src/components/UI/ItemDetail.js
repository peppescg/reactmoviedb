import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFilm,faStar, faTv } from '@fortawesome/free-solid-svg-icons';
import type { ItemSearchType } from '../types/appTypes';

type ItemDetailType = {
    classes: Object,
    item: ItemSearchType,
    baseUrlImg: string
};

const ItemDetail = ({ classes, item: { poster_path = 'wwemzKWzjKYJFfCeiB57q3r4Bcm.png' }, item, baseUrlImg }: ItemDetailType) => (
    <article className={classes.itemWrapper}>
        <section>
            <picture>
                {poster_path && <source media="(min-width: 650px)" srcSet={`${baseUrlImg}/${poster_path}`}/>}
                {poster_path && <img className={classes.image} src={`${baseUrlImg}/${poster_path}`} alt={item.title}/>}
            </picture>
            <header>
                <h3 className={classes.iconMovieTitle}>{item.title || item.name}</h3>
                <div className={classes.movieVote}>
                    { item.media_type === 'movie' && <FontAwesomeIcon icon={faFilm}/>}
                    { item.media_type === undefined && <FontAwesomeIcon icon={faStar}/> }
                    { item.media_type === 'tv' && <FontAwesomeIcon icon={faTv}/> }
                    <h6> Vote average: {item.vote_average}</h6>
                </div>
            </header>
        </section>
        <div className={classes.movieOverview}>
            <small>{item.overview}</small>
        </div>
    </article>
);

export default ItemDetail;