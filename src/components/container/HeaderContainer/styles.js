export const styles = {
    headerWrapper: {
        backgroundColor: '#081c24',
        color: '#01D277',
        margin: 0,
        display: 'flex',
        alignItems: 'center',
        borderBottom: '1px solid #01D277',
        position: 'fixed',
        width: '100%',
        justifyContent: 'center'
    },
    logoWrapper: {
        marginLeft: 50,
        display: 'flex',
        alignItems: 'center',
        '& > a > h1': {
            margin: 0,
        },
        '& > a': {
            color: 'inherit',
            textDecoration: 'none'
        }
    },
    '@media (max-width: 767px)': {
        headerWrapper: {
            flexDirection: 'column'
        },
        logoWrapper: {
            padding: 0
        }
    },
};