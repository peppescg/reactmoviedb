import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import type { ItemSearchType } from '../types/appTypes';

type PersonItemType = {
    classes: Object,
    item: ItemSearchType,
    baseUrlImg: string
};

const PersonItem = ({ classes, item: { profile_path = 'wwemzKWzjKYJFfCeiB57q3r4Bcm.png' }, item, baseUrlImg }: PersonItemType) => (
    <article className={classes.itemWrapper}>
        <header>
            <h3 className={classes.person}>
                <FontAwesomeIcon icon={faUser}/>
                <span> {item.name}</span>
            </h3>
        </header>
        <picture>
            {profile_path && <source media="(min-width: 650px)" srcSet={`${baseUrlImg}/${profile_path}`}/>}
            {profile_path &&
            <img className={classes.actorsImage} src={`${baseUrlImg}/${profile_path}`} alt={item.title}/>}
        </picture>
    </article>
);

export default PersonItem;