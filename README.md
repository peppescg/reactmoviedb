***MovieDB App***

**Command to run application**

**NODE VERSION MANDATORY >= 8.9.4 < 9.0.0**

1) *yarn ( install dependencies into node_modules )*

2) *yarn start for development mode ( autorun on localhost:8080)*

3) *yarn build & yarn serve for production mode*


**App description**

This application was developed for fetch and display data from the movie db website exposed by https://developers.themoviedb.org/3.
There are 2 routes *Home* and *Detail*, the first represented the movies sorted by best vote average, if possible to see the detail about a movie on click.
The second route is triggered by click on item, both home items and search engine popup.

The search bar component has some handlers for trigger onChange, onClick, onKeyUp, onSubmit events, at the beginning the popup display the trending weekly items.
Every changes on input field trigger a call to search/multi api that find any occurrences for the query.


**Stack Tech**


I used react v16.4.2, because I think it is really simple for development html components, I also used react-router 4 and react-jss for stylesheet,
I think that a view component must be simple so it no needed a lot of css or preprocessor like sass, less.

For typing I used flow, I think flow it is not good in production mode for a big projects, because there a lot of bad stuff and inconsistencies, I prefer typescript but not for this project because it is too strict and I would have lost too much time.

For eslint I used some rules that I used for my project LeroyMerlin. The test are made with jest and enzyme.

I used babel 7 as transpiler and webpack 4 as task runner and minify

There are a .env file for dynamically load baseUrl, apiKey or node env.


**Next step**

I think that there a lot of features that it need, the error boundary for catch react errors component, increase the consistency of tests and the coverage, I made only one test for component type and test Endpoint class.
If I have time I refactor the popup search bar, there are too container and sub components, it is not necessary and it increases the complexity.
Another feature that I would like to do it is the last query search using sessionStorage, I can save the queries, only the last 3 and display on popup opening.
It good to have a state management app to share data through different component and exposing the app state to window.
There isn't a post endpoint so another think is to implement an api for rate tv show.
Others implementation could be code splitting, tree shaking and components chunk as performance features.
Now the minify is only for js, not for html and css.
It is possible to separate app js bundle from vendor js of externals library.
Other feature should be lazy loading images and loader before data fetched
