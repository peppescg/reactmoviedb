import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import type { ItemSearchType } from '../types/appTypes';

type GalleryItemCardType = {
  classes: Object,
  item: ItemSearchType,
  baseUrlImg: string
};

const GalleryItemCard = ({ classes , item: { poster_path = 'wwemzKWzjKYJFfCeiB57q3r4Bcm.png' }, item, baseUrlImg }: GalleryItemCardType) => (
  <article className={classes.card}>
    <picture>
      {poster_path && <source media="(min-width: 650px)" srcSet={`${baseUrlImg}${poster_path}`}/>}
      {poster_path && <img className={classes.image} src={`${baseUrlImg}${poster_path}`} alt={item.title}/> }
    </picture>
    <header className={classes.titleContainer}>
      <h4>
        {item.title}
        <span className={classes.average}><FontAwesomeIcon icon={faStar}/> {item.vote_average}</span>
      </h4>
      {item.release_date && <small>Released on <time>{item.release_date}</time></small>}
    </header>
  </article>
);
export default GalleryItemCard;
