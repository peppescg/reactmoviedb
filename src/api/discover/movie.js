import Endpoint from '../Endpoint';

const groups = 'discover';
const action = 'movie';

// https://developers.themoviedb.org/3/discover/movie-discover for api signature
const movie = () => {
    // retrieve env
    const baseUrl = process.env.MOVIE_DB_BASE_URL;
    const apiKey = process.env.MOVIE_DB_API_KEY;
    const pathParams = [groups, action];

    const configAPI = {
        baseUrl: baseUrl,
        apiKey: apiKey,
        pathParams: pathParams
    };
    const endpoint = new Endpoint(configAPI);

    const queryParams = {
        language: 'en-US',
        sort_by: 'vote_average.desc',
        page: 1,
    };


    return endpoint.get(queryParams)
        .then((resolve) => {
            return resolve;
        })
        .catch((reject) => {
            return reject;
        });
};

export default movie;
