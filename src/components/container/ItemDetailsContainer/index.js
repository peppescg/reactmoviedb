import { withRouter } from 'react-router-dom';
import ItemDetailsContainer from './ItemDetailsContainer'

export default withRouter(ItemDetailsContainer);
