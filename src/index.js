import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import App from './components/App';

const wrapper = document.getElementById('container');
wrapper ? ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, wrapper) : false;

