import Endpoint from '../Endpoint';

const groups = 'trending';
const action = 'all';
const time = 'week';

// https://developers.themoviedb.org/3/trending/get-trending signature for trending all api
const all = () => {
    // retrieve env
    const baseUrl = process.env.MOVIE_DB_BASE_URL;
    const apiKey = process.env.MOVIE_DB_API_KEY;
    const pathParams = [groups, action, time];

    const configAPI = {
        baseUrl: baseUrl,
        apiKey: apiKey,
        pathParams: pathParams
    };
    const endpoint = new Endpoint(configAPI);

    return endpoint.get()
        .then((resolve) => {
            return resolve;
        })
        .catch((reject) => {
            return reject;
        });
};

export default all;
