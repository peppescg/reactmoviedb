const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const InterpolateHtmlPlugin = require('interpolate-html-plugin');
const Dotenv = require('dotenv-webpack');

const envPlugin = new Dotenv();
const interpolateHtmlPlugin = new InterpolateHtmlPlugin({
    PUBLIC_URL: '',
});
const htmlWebpackPlugin = new HtmlWebPackPlugin({
    template: './public/index.html',
    filename: './index.html',
});
const copyWebpackPlugin = new CopyWebpackPlugin([{
    from: './public/index.css',
    to: './index.css',
}]);


module.exports = {
    mode: 'development',
    entry: {
        index: './src/index.js',
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '',
        library: 'reactJss',
        libraryTarget: 'umd',
    },
    devServer: {
        historyApiFallback: true,
    },
    devtool: 'cheap-module-eval-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: { loader: 'babel-loader' },
            },
            {
                test: /\.html$/,
                use: [{ loader: 'html-loader' }],
            }, {
                test: /\.css$/,
                loader: 'style-loader!css-loader',
            }, {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader',
                ],
            }],
    },
    plugins: [
        htmlWebpackPlugin,
        copyWebpackPlugin,
        interpolateHtmlPlugin,
        envPlugin,
    ],
};