import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch, faTimes} from '@fortawesome/free-solid-svg-icons';
import TrendApi from '../../../api/trending/all';
import MultiAPI from '../../../api/search/multi';
import SearchListContainer from './SearchListContainer';
import type {ItemSearchType} from '../../types/appTypes';

const SEARCH_CONSTANT = {
    placeholder: 'Search movies, series, actors...',
};

type SearchPopupContainerStateTypes = {
    popupIsOpen: boolean,
    totalResults: number | null,
    totalPages: number | null,
    searchType: string,
    value: string,
    results?: Array<ItemSearchType>
};

/*
    This container component manage the search bar and handle the events for call api
 */
class SearchPopupContainer extends Component<{}, SearchPopupContainerStateTypes> {

    constructor(props: React.Node) {
        super(props);
        this.state = {
            value: SEARCH_CONSTANT.placeholder,
            popupIsOpen: false,
            results: [],
            totalResults: null,
            totalPages: null,
            searchType: '',
        };
        //$FlowFixMe
        this.handleChange = this.handleChange.bind(this);
        //$FlowFixMe
        this.handleSubmit = this.handleSubmit.bind(this);
        //$FlowFixMe
        this.handleOpenSearchPopup = this.handleOpenSearchPopup.bind(this);
        //$FlowFixMe
        this.handleClickOutside = this.handleClickOutside.bind(this);
        //$FlowFixMe
        this.handleKeyUp = this.handleKeyUp.bind(this);
        //$FlowFixMe
        this.handleDeleteQuery = this.handleDeleteQuery.bind(this);
        //$FlowFixMe
        this.handleClosePopup = this.handleClosePopup.bind(this);
        //$FlowFixMe
        this.setWrapperRef = this.setWrapperRef.bind(this);
    }

    /*
        call Api Trend, set searchType on state
        save the results to component state
     */
    trendSearch() {
        this.setState({searchType: 'trend'});
        TrendApi()
            .then((resultsFetched) => {
                this.setState({
                    results: resultsFetched.results,
                    totalResults: resultsFetched.total_results,
                    totalPages: resultsFetched.total_pages,
                });
            })
            .catch((reject) => {
                console.error('Error trend api ', reject);
            });
    }

    /*
      call Api Multi search, set searchType on state,
      save the results to component state
     */
    searchQuery(query: string) {
        this.setState({ searchType: 'search' });
        MultiAPI(query)
            .then((resultsFetched) => {
                this.setState({
                    results: resultsFetched.results,
                    totalResults: resultsFetched.total_results,
                    totalPages: resultsFetched.total_pages,
                });
            })
            .catch((reject) => {
                console.error('Error multi search api ', reject);
            });
    }

    /*
    * handleChange for call trendSearch method
    * */
    handleChange(event: SyntheticKeyboardEvent<HTMLInputElement>) {
        //$FlowFixMe
        const query = event.target.value;
        this.setState({value: query});
        if (query) {
            this.searchQuery(query);
        } else {
            this.trendSearch();
        }
    }

    //handleKeyUp ESC for popup closing
    handleKeyUp(e: SyntheticKeyboardEvent<HTMLElement>) {
        if (e.keyCode === 27) {
            this.setState({popupIsOpen: false});
            this.setState({value: ''});
        }
    }

    handleDeleteQuery(e: SyntheticEvent<HTMLElement>) {
        this.setState({ popupIsOpen: false });
        this.setState({ value: SEARCH_CONSTANT.placeholder });
    }

    handleSubmit(event: SyntheticMouseEvent<HTMLElement>) {
        //$FlowFixMe
        const query = event.target.value;
        if (query) {
            this.searchQuery(query);
        } else {
            this.trendSearch();
        }
    }

    /*
    * handleClick for open popup
    * call method trend search if needed
    */
    handleOpenSearchPopup(e: SyntheticInputEvent<HTMLInputElement>) {
        if ((this.state.value === e.target.value || e.target.value === '') && !this.state.totalResults) {
            this.setState({value: ''});
            this.trendSearch();
        }
        if (e.target.value === SEARCH_CONSTANT.placeholder) {
            this.setState({value: ''});
        }
        this.setState({popupIsOpen: true});
    }

    // checking click event html elements for closing popup
    handleClickOutside(event: SyntheticMouseEvent<HTMLElement>) {
        //$FlowFixMe
        if (this.wrapperRef && this.state.popupIsOpen && !this.wrapperRef.contains(event.target) && event.target.type !== 'text') {
            this.setState({popupIsOpen: false});
            this.setState({value: SEARCH_CONSTANT.placeholder});
        }
    }

    // add eventListener for click outside results popup container
    componentDidMount() {
        //$FlowFixMe
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    // unmount eventListener for click outside results popup container
    componentWillUnmount() {
        //$FlowFixMe
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClosePopup() {
        this.setState({ value: SEARCH_CONSTANT.placeholder });
        this.setState({ popupIsOpen: false });
    }

    setWrapperRef(node: HTMLElement) {
        this.wrapperRef = node;
    }

    render() {
        const { classes } = this.props;
        const { popupIsOpen, totalPages, totalResults, results, searchType } = this.state;
        return (
            <form className={classes.formWrapper}>
                <div className={classes.searchForm}>
                    <FontAwesomeIcon icon={faSearch} className={classes.searchSubmitIcon} onClick={this.handleSubmit}/>
                    <input
                        type="text"
                        value={this.state.value}
                        onClick={this.handleOpenSearchPopup}
                        onKeyUp={this.handleKeyUp}
                        onChange={this.handleChange}/>
                    <FontAwesomeIcon icon={faTimes} className={classes.searchDeleteIcon}
                                     onClick={this.handleDeleteQuery}/>
                </div>
                <div ref={this.setWrapperRef}>
                    {popupIsOpen &&
                    <SearchListContainer page={totalPages} totalResults={totalResults} resultsItem={results}
                                         totalPages={totalPages} searchType={searchType} closePopup={this.handleClosePopup}/>}
                </div>
            </form>
        );
    }
}

export default SearchPopupContainer;
