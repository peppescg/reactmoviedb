export const styles = {
    detailContainer: {
        height: 500
    },
    itemWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'center',
        '& > section > header': {
            display: 'flex',
            justifyContent: 'space-between'
        }
    },
    movieVote: {
        display: 'flex',
        '& > h6': {
            margin: 0,
            paddingLeft: 20
        },
    },
    iconMovieTitle: {
        margin: 0,
    },
    movieOverview: {
        margin: '0 20% 0 20%'
    }
};