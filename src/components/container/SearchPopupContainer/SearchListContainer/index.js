import { withRouter } from 'react-router-dom';
import SearchListContainer from './SearchListContainer';

export default withRouter(SearchListContainer);