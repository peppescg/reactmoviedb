export const styles = {
    itemCard: {
        boxShadow: '0px 4px 20px 0px rgba(0,0,0,0.1)',
        transition: '0.3s',
        width: 300,
        display: 'flex',
        '&:hover': {
            boxShadow: '0 8px 16px 0 rgba(0,0,0,0.2)',
        },
        padding: '5px 25px 5px 25px',
        cursor: 'pointer',
        margin: '5px 15px 5px 0',
    },
    searchListWrapper: {
        width: '100%',
        height: 550,
        backgroundColor: '#ffffff',
        position: 'absolute',
        left: 0,
        overflowY: 'hidden'
    },
    resultsArticle: {
        overflowX: 'hidden',
        marginTop: 0,
        listStyle: 'none',
        overflowY: 'scroll',
        height: 500,
        transition: '0.3s',
        backgroundImage: 'linear-gradient(#ffffff, #d6e2d7)',
        filter: 'progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#ffffff\', endColorstr=\'#01D277\',GradientType=0 )',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        padding: '0px 50px 0 30px',
    },
    totalResults: {
        margin: '10px 0 5px 40px',
        justifyContent: 'space-between',
        display: 'flex',
        width: 300,
    },
    searchingType: {
        paddingLeft: 20,
    },
    detailSearching: {
        display: 'flex',
        alignItems: 'center',
        height: 35,
    },
    '@media (max-width: 767px)': {
        resultsArticle: {
            padding: '0px 0px 0 0px',
        }
    },
    '@media (min-width: 768px)': {
        resultsArticle: {
            padding: '0px 0px 0 0px',
        }
    },
};