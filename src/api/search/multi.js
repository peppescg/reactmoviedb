import Endpoint from '../Endpoint';

const groups = 'search';
const action = 'multi';

// https://developers.themoviedb.org/3/search/multi-search signature for multi search api
const multi = (query: string) => {
    // retrieve env
    const baseUrl = process.env.MOVIE_DB_BASE_URL;
    const apiKey = process.env.MOVIE_DB_API_KEY;
    const pathParams = [groups, action];

    const configAPI = {
        baseUrl: baseUrl,
        apiKey: apiKey,
        pathParams: pathParams
    };
    const endpoint = new Endpoint(configAPI);

    const queryParams = {
        language: 'en-US',
        query: query.trim(),
        page: 1
    };


    return endpoint.get(queryParams)
        .then((resolve) => {
            return resolve;
        })
        .catch((reject) => {
            return reject;
        });
};

export default multi;
