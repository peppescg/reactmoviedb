import React, {Component} from 'react';
import {  Link } from 'react-router-dom';
import injectSheet from 'react-jss';
import SearchPopupContainer from '../SearchPopupContainer/index';
import { styles } from './styles';

class HeaderContainer extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.headerWrapper}>
                <hgroup className={classes.logoWrapper}>
                    <Link to="/"><h1>MovieDB</h1></Link>
                </hgroup>
                <SearchPopupContainer />
            </div>

        );
    }
}
export default injectSheet(styles)(HeaderContainer);
