import Endpoint from '../Endpoint';


describe('Create Endpoint instance', () => {
    let expectedInstance, config, queryParams;

    beforeEach(() => {
        expectedInstance = {
            endpointURL: 'baseUrl/trending/all',
            apiKey: 'apiKey',
            getConfig: {
                method: 'GET',
                mode: 'cors',
                cache: 'default'
            },
        };
        config = {
            apiKey: 'apiKey',
            baseUrl: 'baseUrl',
            pathParams: [
                'trending',
                'all',
            ],
        };
        queryParams = {
            query:'brad',
            language: 'en-US',
            page: 1
        };
    });

    test('SUCCESS instance creation with base url', () => {
        const endpoint = new Endpoint(config);
        expect(endpoint.endpointURL).toEqual(expectedInstance.endpointURL);
        expect(endpoint.apiKey).toEqual(expectedInstance.apiKey);
    });

    test('SUCCESS get call with queryParams builder', () => {
        const endpoint = new Endpoint(config);
        fetch.mockResponseOnce(JSON.stringify({ data: 'example' }));

        endpoint.get(queryParams).then(res => {
            expect(res.data).toEqual('example');
        });
        expect(fetch.mock.calls.length).toEqual(1);
        expect(fetch.mock.calls[0][0]).toEqual('baseUrl/trending/all?api_key=apiKey&query=brad&language=en-US&page=1&');
    });

});
