import React, {Component, PureComponent} from 'react';
import injectSheet from 'react-jss';
import {styles} from './styles';
import SearchItemContainer from './SearchItemContainer';

class SearchListContainer extends PureComponent {

    constructor(props: React.Node) {
        super(props);
        //$FlowFixMe
        this.handleItemClick = this.handleItemClick.bind(this);
    }

    handleItemClick(item) {
        this.props.closePopup();
        this.props.history.push({
            pathname: '/detail',
            state:  item
        });
    }

    render() {
        const {classes, resultsItem, totalResults, totalPages, searchType } = this.props;
        const listItems = resultsItem && resultsItem.map((item) =>
            <li key={item.id} className={classes.itemCard} onClick={() => this.handleItemClick(item)} >
                <SearchItemContainer itemInfo={item}/>
            </li>
        );
        const type = searchType === 'search' ? 'Generical search' : 'Weekly trends';
        return (
            <div className={classes.searchListWrapper}>
                <div className={classes.detailSearching}>
                    <div className={classes.totalResults}>
                        {totalResults && <span>Total Results: {totalResults}</span>}
                        {totalPages && <span>Total Results: {totalPages}</span>}
                    </div>
                    <div className={classes.searchingType}>
                        <h2>{type}</h2>
                    </div>
                </div>
                <div>
                    <ul className={classes.resultsArticle}>{listItems}</ul>
                </div>
            </div>
        );
    }
}
export default injectSheet(styles)(SearchListContainer);
