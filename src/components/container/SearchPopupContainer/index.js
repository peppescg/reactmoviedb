import injectSheet from 'react-jss';
import { styles } from './styles';
import SearchPopupContainer from './SearchPopupContainer';

export default injectSheet(styles)(SearchPopupContainer);
