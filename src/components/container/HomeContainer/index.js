import HomeContainer from './HomeContainer'
import { withRouter } from 'react-router-dom';

export default  withRouter(HomeContainer);
