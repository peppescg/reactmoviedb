export const styles = {
    image: {
        width: 100,
        paddingRight: 5
    },
    actorsImage: {
        width: 260,
    },
    itemWrapper: {
        '& > section': {
            display: 'flex',
        }
    },
    person: {
        '& > span': {
            paddingLeft: 10,
        },
    },
    iconTvTitle: {
        '& > span': {
            paddingLeft: 10,
        },
        marginBottom: 5,
    },
    iconMovieTitle: {
        margin: 0,
        '& > span': {
            paddingLeft: 10,
        },
        marginBottom: 5,
    },
    movieVote: {
        display: 'flex',
        '& > h6': {
            margin: 0,
            paddingLeft: 5
        },
    },
    tvVote: {
        '& > h6': {
            marginTop: 5,
        },
    },
};