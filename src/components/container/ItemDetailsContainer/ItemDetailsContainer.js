import React, { Component, PureComponent } from 'react';
import { styles } from './styles';
import injectSheet from 'react-jss';
import ItemDetail from '../../UI/ItemDetail'

const imgBaseUrl = process.env.MOVIE_IMG_BASE_URL;

class ItemDetailsContainer extends PureComponent {
  render() {
    const { classes, location: { state = {} } } = this.props;
    return (
      <React.Fragment>
        <ItemDetail className={classes.detailContainer} classes={classes} item={state} baseUrlImg={imgBaseUrl} />
      </React.Fragment>
    );
  }
}

export default injectSheet(styles)(ItemDetailsContainer);