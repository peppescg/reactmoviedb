import React from 'react';
import Enzyme, { render, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SearchPopupContainer from '../SearchPopupContainer';

Enzyme.configure({ adapter: new Adapter() });

describe('Test SearchPopupContainer ', () => {
    let props;
    beforeEach(() => {
        props = {
            popupIsOpen: false,
            results: [],
            totalResults: null,
            totalPages: null,
            searchType: '',
            value: 'placeholder',
            handleOpenSearchPopup: jest.fn(() => 'handleOpenSearchPopup'),
            handleChange: jest.fn(() => 'handleChange'),
            handleSubmit: jest.fn(() => 'handleSubmit'),
            handleClickOutside: jest.fn(() => 'handleClickOutside'),
            handleKeyUp: jest.fn(() => 'handleKeyUp'),
            handleDeleteQuery: jest.fn(() => 'handleDeleteQuery'),
            handleClosePopup: jest.fn(() => 'handleClosePopup'),
            setWrapperRef: jest.fn(() => 'setWrapperRef'),
        };

    });

    describe('Open popup use cases', () => {

        test('Success open popup', () => {
            const wrapper = shallow(<SearchPopupContainer classes={{}}/>);

            wrapper.find('input').simulate('click', {
                target: {
                    value: 'Search movies, series, actors...'
                }
            });

            expect(wrapper.state('popupIsOpen')).toEqual(true);
            expect(wrapper.state('value')).toEqual('');
        });

        test('Success open popup, call trendSearch api ', () => {
            const wrapper = shallow(<SearchPopupContainer classes={{}}/>);

            wrapper.find('input').simulate('click', {
                target: {
                    value: 'Search movies, series, actors...'
                }
            });

            wrapper.find('input').simulate('change', { target: {
                    value: 'm'
                }
            });

            expect(wrapper.state('value')).toEqual('m');
            expect(wrapper.state('searchType')).toEqual('search');
        });


    });

});