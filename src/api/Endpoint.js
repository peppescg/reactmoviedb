// build the query params of api with or without query params
const builderQueryParams = (queryParams, endpointURL, apiKey) => {

    if (!queryParams)
        return `${endpointURL}?api_key=${apiKey}`;

    let queryParamsUrl = '';
    Object.keys(queryParams).reduce((prev, next) => {
        queryParamsUrl += `${next}=${queryParams[next]}&`;
        return queryParamsUrl;
    }, {});

    return `${endpointURL}?api_key=${apiKey}&${queryParamsUrl}`;
};

// build endpoint url from base url and pathParmas group action of an api
const buildEndpointURL = (baseUrl, pathParams) => {
    const pathParamsJoin = pathParams.join();
    return `${baseUrl}/${pathParamsJoin.replace(/,/g, '/')}`;
};

// singleton class layer for fetch data with http get method
export default class Endpoint {

    baseUrl: string;
    apiKey: string;
    getConfig = {
        method: 'GET',
        mode: 'cors',
        cache: 'default',
    };
    postConfig = {
        method: 'POST',
        mode: 'cors',
        cache: 'default',
    };
    pathParams: Object;
    endpointURL: string;

    constructor(config: Object) {
        this.baseUrl = config.baseUrl;
        this.apiKey = config.apiKey;
        this.pathParams = config.pathParams;
        this.endpointURL = buildEndpointURL(this.baseUrl, this.pathParams);
    }

    async get(queryParams?: Object, pathParams?: Object) {
        const url = builderQueryParams(queryParams, this.endpointURL, this.apiKey);

        // await response of fetch call
        let response = await fetch(url, this.getConfig);
        // only proceed once promise is resolved
        let data = await response.json();
        // only proceed once second promise is resolved
        return data;
    }
}

