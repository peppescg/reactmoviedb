import injectSheet from 'react-jss'
import { styles } from './styles';
import SearchItemContainer from './SearchItemContainer';

export default injectSheet(styles)(SearchItemContainer);